<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

get_header(); 

wp_enqueue_style( 'leaflet_style', get_template_directory_uri() . '/dist/leaflet/leaflet.css');
wp_enqueue_style( 'onirama_style', get_template_directory_uri() . '/onirama/style.css');
wp_enqueue_script( 'leaflet', get_template_directory_uri() . '/dist/leaflet/leaflet.js');
wp_enqueue_script( 'leafletcurve', get_template_directory_uri() . '/dist/leaflet/leaflet.curve.js');
wp_enqueue_script( 'leafmarkercluster', get_template_directory_uri() . '/dist/markercluster/leaflet.markercluster.js');
wp_enqueue_script( 'jquerymobile', get_template_directory_uri() . '/dist/jquerymobile/jquery.mobile.custom.min.js');
wp_enqueue_style( 'leafmarkercluster_style', get_template_directory_uri() . '/dist/markercluster/MarkerCluster.css');
wp_enqueue_style( 'leafmarkerclusterdefault_style', get_template_directory_uri() . '/dist/markercluster/MarkerCluster.Default.css');

wp_enqueue_script( 'onirama', get_template_directory_uri() . '/onirama/onirama.js');
wp_localize_script('onirama', 'WPURLS', array( 'siteurl' => get_option('siteurl') ));
wp_enqueue_script( 'onirama');
?>

<div id="oni-header">
    <?php
    twentyfifteen_the_custom_logo();
    
    $description = get_bloginfo( 'description', 'display' );
    ?>

    <div id="oni-header-btn-map" class="oni-header-btn">

    </div>

    <div id="oni-header-btn-i" class="oni-header-btn">

    </div>

</div>

<div id="primary" class="content-area">
    <div id="mapid"></div>
</div> <!-- .content-area -->

<div id="page-content" class="oni-single-post">

    <?php
    // Start the loop.
    while ( have_posts() ) : the_post();
    $thepost = get_post();
    echo '<div class="oni-postname oni-data">' . $thepost->post_name . '</div>'.PHP_EOL;
    /*
     * Include the post format-specific template for the content. If you want to
     * use this in a child theme, then include a file called content-___.php
     * (where ___ is the post format) and that will be used instead.
     */
    get_template_part( 'content', get_post_format() );
    
    // If comments are open or we have at least one comment, load up the comment template.
    if ( comments_open() || get_comments_number() ) :
	 comments_template();
    endif;
    
    // Previous/next post navigation.
    /* the_post_navigation( array(
       'next_text' => '<span class="meta-nav" aria-hidden="true">' . __( 'Next', 'twentyfifteen' ) . '</span> ' .
       '<span class="screen-reader-text">' . __( 'Next post:', 'twentyfifteen' ) . '</span> ' .
       '<span class="post-title">%title</span>',
       'prev_text' => '<span class="meta-nav" aria-hidden="true">' . __( 'Previous', 'twentyfifteen' ) . '</span> ' .
       '<span class="screen-reader-text">' . __( 'Previous post:', 'twentyfifteen' ) . '</span> ' .
       '<span class="post-title">%title</span>',
     * ) );
     * */
    // End the loop.
    endwhile;
    ?>
    
</div><!-- #page-content -->

<div id="oni-posts">
    <?php    
    $posts = get_posts('numberposts', -1);
    foreach ( $posts as $post) :?>
	<?php 
	$thepost = get_post($post); 
	the_post();
	?>
	
	<?php if (get_field("latitude", $thepost)): ?>
	    <?php if (!has_tag("bacasable")): ?>
		
		<div class="oni-post">
		    <div class="oni-lat oni-data"><?php echo get_field("latitude", $thepost); ?></div>
		    <div class="oni-long oni-data"><?php echo get_field("longitude", $thepost); ?></div>
		    <div class="oni-id oni-data"><?php echo get_the_id(); ?></div>
		    <div class="oni-postname oni-data"><?php echo $thepost->post_name; ?></div>
		    <div class="oni-posttype oni-data">
			<?php if (get_post_format($thepost)): ?>
			    <?php echo get_post_format($thepost) ?>
			<?php endif ?>
		    </div>
		    
		</div>
	    <?php endif ?>
	<?php endif ?>
	
    <?php endforeach; ?> 
    
    <?php
    $page = get_page_by_title("L'Aquarius");
    $title = $page->post_title;
    $content = apply_filters('the_content', $page->post_content);
    ?>
    
    <div id="oni-bateau" class="oni-post">
	<div class="oni-id oni-data">0</div>
	<div class="oni-postname oni-data"><?php echo $page->post_name; ?></div>
	<div class="oni-posttype oni-data">bateau</div>
    </div>

    <?php
    $page = get_page_by_title("GPS");
    $title = $page->post_title;
    $content = apply_filters('the_content', $page->post_content);

    ?>

    <div id="oni-gps" class="oni-data">
	<?php echo $content; ?>
    </div>
    
</div>

<?php get_footer(); ?>
