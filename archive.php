<?php
/**
 * The template for displaying archive pages
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * If you'd like to further customize these archive views, you may create a
 * new template file for each one. For example, tag.php (Tag archives),
 * category.php (Category archives), author.php (Author archives), etc.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

get_header();

wp_enqueue_style( 'leaflet_style', get_template_directory_uri() . '/dist/leaflet/leaflet.css');
wp_enqueue_style( 'onirama_style', get_template_directory_uri() . '/onirama/style.css');
wp_enqueue_script( 'leaflet', get_template_directory_uri() . '/dist/leaflet/leaflet.js');
wp_enqueue_script( 'leafletcurve', get_template_directory_uri() . '/dist/leaflet/leaflet.curve.js');
wp_enqueue_script( 'leafmarkercluster', get_template_directory_uri() . '/dist/markercluster/leaflet.markercluster.js');
wp_enqueue_script( 'jquerymobile', get_template_directory_uri() . '/dist/jquerymobile/jquery.mobile.custom.min.js');
wp_enqueue_style( 'leafmarkercluster_style', get_template_directory_uri() . '/dist/markercluster/MarkerCluster.css');
wp_enqueue_style( 'leafmarkerclusterdefault_style', get_template_directory_uri() . '/dist/markercluster/MarkerCluster.Default.css');

wp_enqueue_script( 'onirama', get_template_directory_uri() . '/onirama/onirama.js');
wp_localize_script('onirama', 'WPURLS', array( 'siteurl' => get_option('siteurl') ));
wp_enqueue_script( 'onirama');

?>

<script type="text/javascript">
 var templateUrl = '<?= get_bloginfo("template_url"); ?>';
</script>

<div id="oni-header">
    <?php
    twentyfifteen_the_custom_logo();
    
    $description = get_bloginfo( 'description', 'display' );
    ?>

    <div id="oni-header-btn-map" class="oni-header-btn">

    </div>

    <div id="oni-header-btn-i" class="oni-header-btn">

    </div>

</div>
<div id="primary" class="content-area">
    <div id="mapid"></div>
</div>


<div id="page-content" class="oni-main" role="main">
    
    <?php if ( have_posts() ) : ?>
	
	<header class="page-header">
	    <?php
	    the_archive_title( '<h1 class="page-title">', '</h1>' );
	    the_archive_description( '<div class="taxonomy-description">', '</div>' );
	    ?>
	</header><!-- .page-header -->
	
	<?php
	// Start the Loop.
	while ( have_posts() ) : the_post();
	
	/*
	 * Include the Post-Format-specific template for the content.
	 * If you want to override this in a child theme, then include a file
	 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
	 */
	get_template_part( 'content', get_post_format() );
	
	// End the loop.
	endwhile;
	
	// Previous/next page navigation.
	/* the_posts_pagination( array(
	   'prev_text'          => __( 'Previous page', 'twentyfifteen' ),
	   'next_text'          => __( 'Next page', 'twentyfifteen' ),
	   'before_page_number' => '<span class="meta-nav screen-reader-text">' . __( 'Page', 'twentyfifteen' ) . ' </span>',
	   ) );
	 */
	// If no content, include the "No posts found" template.
	else :
	get_template_part( 'content', 'none' );
	
	endif;
	?>

</div><!-- .site-main -->
	


<div id="oni-posts">
    <?php    
    $posts = get_posts('numberposts', -1);
    foreach ( $posts as $post) :?>
	<?php 
	$thepost = get_post($post); 
	the_post();
	?>
	
	
	<?php if (get_field("latitude", $thepost)): ?>
	    <?php if (!has_tag("bacasable")): ?>
	    <div class="oni-post">
		<div class="oni-lat oni-data"><?php echo get_field("latitude", $thepost); ?></div>
		<div class="oni-long oni-data"><?php echo get_field("longitude", $thepost); ?></div>
		<div class="oni-id oni-data"><?php echo get_the_id(); ?></div>
		<div class="oni-postname oni-data"><?php echo $thepost->post_name; ?></div>
		<div class="oni-posttype oni-data">
		    <?php if (get_post_format($thepost)): ?>
			<?php echo get_post_format($thepost) ?>
		    <?php endif ?>
		</div>


		<div class="oni-content">
		    <?php 

		    get_template_part( 'content', get_post_format() );
		    ?>
		    <!-- 
			 <p "post-title"><?php echo get_the_title($thepost); ?> </p>

			 <?php echo get_post_field('post_content', $thepost);?> -->
		</div>
	    </div>
	<?php endif ?>
	<?php endif ?>
    <?php endforeach; ?> 
    <?php
    $page = get_page_by_title("L'Aquarius");
    $title = $page->post_title;
    $content = apply_filters('the_content', $page->post_content);
    ?>
    
    <div id="oni-bateau" class="oni-post">
	<div class="oni-id oni-data">0</div>
	<div class="oni-postname oni-data"><?php echo $page->post_name; ?></div>
	<div class="oni-posttype oni-data">bateau</div>
	
	<div class="oni-content">
	    <article class="hentry">
					   
		<h2 class="entry-title">
		    <a href="<?php echo get_page_link($page); ?>">
			<?php echo $title;?>
		    </a>
		</h2>
		
		<?php echo $content; ?>
	</article>
	</div>
    </div>

    <?php
    $page = get_page_by_title("GPS");
    $title = $page->post_title;
    $content = apply_filters('the_content', $page->post_content);

    ?>

    <div id="oni-gps" class="oni-data">
	<?php echo $content; ?>
    </div>
</div>


	
	<?php get_sidebar(); ?>
	<?php get_footer(); ?>

