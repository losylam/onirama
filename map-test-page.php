<?php
/**
/**
 * Template Name: Map Test Page
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */


get_header();


wp_enqueue_style( 'leaflet_style', get_template_directory_uri() . '/dist/leaflet/leaflet.css');
wp_enqueue_style( 'onirama_style', get_template_directory_uri() . '/onirama/style_accueil.css');
wp_enqueue_script( 'leaflet', get_template_directory_uri() . '/dist/leaflet/leaflet.js');
wp_enqueue_script( 'leafletcurve', get_template_directory_uri() . '/dist/leaflet/leaflet.curve.js');
wp_enqueue_script( 'onirama', get_template_directory_uri() . '/onirama/onirama_accueil.js');

// wp_enqueue_script( 'leaflet', get_template_directory_uri() . '/js/script.js'), array ( 'jquery' ), 1.1, true); 
?>

<script type="text/javascript">
 var templateUrl = '<?= get_bloginfo("template_url"); ?>';
</script>


<div id="primary" class="content-area">
    <div id="mapid"></div>
</div> <!-- .content-area -->




<div id="page-content">


    <?php
    // Start the loop.
    while ( have_posts() ) : the_post();

    // Include the page content template.
    get_template_part( 'content', 'page' );

    // If comments are open or we have at least one comment, load up the comment template.
    if ( comments_open() || get_comments_number() ) :
    comments_template();
    endif;

    // End the loop.
    endwhile;
    ?>

</div>

<div id="oni-posts">
    <?php    
    $posts = get_posts('numberposts', -1);
    foreach ( $posts as $post) :?>
	<?php 
	$thepost = get_post($post); 
	the_post();
	?>


	<?php if (get_field("latitude", $thepost)): ?>
	    <div class="oni-post">
		<div class="oni-lat oni-data"><?php echo get_field("latitude", $thepost); ?></div>
		<div class="oni-long oni-data"><?php echo get_field("longitude", $thepost); ?></div>
		
		<div class="oni-posttype oni-data">
		    <?php if (get_post_format($thepost)): ?>
			<?php echo get_post_format($thepost) ?>
		    <?php endif ?>
		</div>


		<div class="oni-content">
		    <?php 


		    get_template_part( 'content', get_post_format() );
		    ?>
		    <!-- 
			 <p "post-title"><?php echo get_the_title($thepost); ?> </p>

			 <?php echo get_post_field('post_content', $thepost);?> -->
		</div>
	    </div>
	<?php endif ?>
    <?php endforeach; ?> 
</div>

<?php get_sidebar(); ?>
<?php get_footer(); ?>
