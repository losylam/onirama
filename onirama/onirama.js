var backmap;
var mymap;
var posts;
var maplayer;

var ll;

var pp;

jQuery(document).ready(function($) {

    posts = {};
 
    var oni_mob_w = 954;
    var oni_mob = false;
    var oni_frontpage = "";

    var bateau_marker;
    
    var oni_main = false;
    
    if ($(document).width() < oni_mob_w){
	oni_mob = true;
    }

    var hash_postname = location.hash.slice(1);
    
    // init map

   // choose map tiles
    var mb_token = "pk.eyJ1IjoibGF1cmVudG1hbHlzIiwiYSI6ImNqZ2NjbjFnNjN6OXEyenBoeDZsYTI3cmMifQ.9TuU9MbZFAhnZwh9xrgU0w"
    
    var tonerUrl = "http://{S}tile.stamen.com/toner/{Z}/{X}/{Y}.png";
    //    var tonerUrl = "https://api.mapbox.com/v4/laurentmalys.9iobprs6/{Z}/{X}/{Y}.png?access_token="+ mb_token;
    //    var tonerUrl = "https://api.mapbox.com/v4/laurentmalys.2bw9mm02/{Z}/{X}/{Y}.png?access_token="+mb_token;

    
//    var tonerUrl = "https://api.mapbox.com/styles/v1/laurentmalys/cjh7h0urq057u2ro2p7gbqgf4/tiles/256/{z}/{x}/{y}@2x?access_token=pk.eyJ1IjoibGF1cmVudG1hbHlzIiwiYSI6ImNqZ2NjbjFnNjN6OXEyenBoeDZsYTI3cmMifQ.9TuU9MbZFAhnZwh9xrgU0w"

//    var tonerUrl = "https://api.mapbox.com/styles/v1/laurentmalys/cjhz95lwb42z02rlkrnqzp2va/tiles/256/{z}/{x}/{y}@2x?access_token=pk.eyJ1IjoibGF1cmVudG1hbHlzIiwiYSI6ImNqZ2NjbjFnNjN6OXEyenBoeDZsYTI3cmMifQ.9TuU9MbZFAhnZwh9xrgU0w";

    var mapboxUrl = "https://api.mapbox.com/v4/laurentmalys.8ssdpfkj/{Z}/{X}/{Y}.png?access_token="+mb_token;

    
    //var tonerUrl = "https://api.mapbox.com/styles/v1/laurentmalys/cjhwpld8q1pui2rnlxzulxta4/tiles/256/{z}/{x}/{y}@2x?access_token=pk.eyJ1IjoibGF1cmVudG1hbHlzIiwiYSI6ImNqZ2NjbjFnNjN6OXEyenBoeDZsYTI3cmMifQ.9TuU9MbZFAhnZwh9xrgU0w"
    var url = tonerUrl.replace(/({[A-Z]})/g, function(s) {
	return s.toLowerCase();
    });    

 var murl = mapboxUrl.replace(/({[A-Z]})/g, function(s) {
	return s.toLowerCase();
    });    
    
    maplayer =L.tileLayer(murl, {
	subdomains: ['','a.','b.','c.','d.'],
	attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>',
	minZoom: 0,
	maxZoom: 13,
	id: 'mapbox.streets'// ,
	// accessToken: 'your.mapbox.access.token'
    });//.addTo(mymap);

    var maplayerzoomed =L.tileLayer(url, {
	subdomains: ['','a.','b.','c.','d.'],
	attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="http://maps.stamen.com">Stamen</a> Toner',
	minZoom: 14,
	maxZoom: 18,
	id: 'stamen'// ,
	// accessToken: 'your.mapbox.access.token'
    });//.addTo(mymap);
 
    
   backmap = new L.LayerGroup();
    

    mymap = L.map('mapid', {drawControl: true, 
			    zoomControl: false,
			    layers: [backmap, maplayer, maplayerzoomed]})
	.setView([47.4, -2.5], 8);
    new L.Control.Zoom({ position: 'bottomright' }).addTo(mymap);


    $('.site-description').html('Carnet de voyage du <a href="' + WPURLS.siteurl+'/le-projet-oniri"><strong>projet Oniri</strong></a>');
     
    $('#mapid').width($('#primary').width());
    $('#mapid').height($('#primary').height());
    mymap.invalidateSize();    

 
    // create path
    var pointList = []

    pointList.push(new L.LatLng(46.145, -1.1782)); //La Rochelle
    pointList.push(new L.LatLng(48.3973101, -4.9570331)); //Molène 
    pointList.push(new L.LatLng(48.03919, -4.84894)); //Sein
    pointList.push(new L.LatLng(47.27479,-2.20272)); //St Nazaire
    pointList.push(new L.LatLng(47.20492,-1.56386)); //Nantes
    
    var larochelle = [46.145, -1.1782];
    var molene = [48.3973101, -4.9570331];
    var sein = [48.03919, -4.84894];
    var saintnazaire = [47.27479,-2.20272];
    var nantes = [47.20492,-1.56386];
    
    function getCurveBetween(latlng1, latlng2, thetaOffset){
	var latlngs = [];
	
	var pathOptions = {
	    color: 'white',
	    weight: 1.5,
	    opacity: 1,
	    smoothFactor: 1,
	    dashArray:"1, 10"
	};

	var offsetX = latlng2[1] - latlng1[1],
	offsetY = latlng2[0] - latlng1[0];
	
	var r = Math.sqrt( Math.pow(offsetX, 2) + Math.pow(offsetY, 2) ),
	theta = Math.atan2(offsetY, offsetX);
	
//	var thetaOffset = (3.14/5.5);
	
	var r2 = (r/2)/(Math.cos(thetaOffset));
	var theta2 = theta + thetaOffset;
	
	var midpointX = (r2 * Math.cos(theta2)) + latlng1[1],
	midpointY = (r2 * Math.sin(theta2)) + latlng1[0];
	
	var midpointLatLng = [midpointY, midpointX];
	
	latlngs.push(latlng1, midpointLatLng, latlng2);

	var curvedPath = L.curve(['M', latlng1,
				  'Q', midpointLatLng,
				  latlng2], pathOptions);
	return curvedPath;
    }

    
    // var l0 = getCurveBetween(larochelle, molene, 3.14/5.5);
    // l0.addTo(mymap);
    // var l1 = getCurveBetween(molene, sein, -3.14/5.5);
    // l1.addTo(mymap);
    // var l2 = getCurveBetween(sein, [47.2389,-2.2110], -3.14/5.5);
    // l2.addTo(mymap);
    // var l2 = getCurveBetween([47.2389,-2.2110],saintnazaire, -3.14/5.5);
    // l2.addTo(mymap);
    // var l2 = getCurveBetween(saintnazaire, [47.2622,-1.8591], 3.14/7);
    // l2.addTo(mymap);
    // var l2 = getCurveBetween([47.2622,-1.8591], nantes, -3.14/9);
    // l2.addTo(mymap);

    // posts and markers
   
    var postIcons = {};
    var posttypes = ["bateau", "video", "audio", "image", "chat", "aside", "quote", "link"];
    var postIconsScale = 0.35;
    var w_icons = 100 * postIconsScale;
    var h_icons = 160 * postIconsScale;
    

    function createPostIcon(posttype, postid, is_selected, oni_elaboreve){
	var cl = 'onimarker';
	if (posttype){
	    cl += ' onimarker-'+ posttype;
	}
	if (postid){
	    cl += ' onimarker-'+postid;
	}
	if (is_selected){
	    cl += ' selected';
	}
	if (oni_elaboreve){
	    cl += ' onimarker-elaboreve';
	}

	var h_anchor = h_icons;
	
	if (posttype == "bateau"){
	    h_anchor = 0;
	}
	
	console.log(cl);
	return L.divIcon({
	    className: cl,
	    iconSize:     [w_icons, h_icons], 
	    iconAnchor:   [w_icons/2, h_anchor]
	});
    }
    
    var oni_posts = $('.oni-post');
    var sidebar = $('#sidebar');
    
    sidebar.append('<div id="oni-post"> </div>') ;
    var onipost = $('#oni-post');
    $('#oni-posts').appendTo(onipost);
    
    var page_content =	$('#page-content');
    page_content.appendTo(sidebar);

    var oni_single_post;
    if (page_content.hasClass('oni-main')){
	oni_main = true;
    }else if(page_content.hasClass('oni-single-post')){
	oni_single_post = page_content.find('.oni-postname').text().trim();
    }
    
    $('#secondary').hide();

    var oni_header_btn_map = $('#oni-header-btn-map')
    var oni_header_btn_i = $('#oni-header-btn-i')

    oni_header_btn_i.on('click', function(){
	oni_posts.each(function(el){
	    $(this).hide();
	});
	
	if (oni_mob){
	    page_content.show();
	    oni_header_btn_i.hide();
	    oni_header_btn_map.fadeIn(500);
	    sidebar.animate({'margin-left':'0px'}, 700);
	}	
    });
    
    function showPost(e){
	page_content.hide();
	
	oni_posts.each(function(el){
	    $(this).hide();
	});
	if (oni_mob){
	    $(e).show();
	    oni_header_btn_i.hide();
	    oni_header_btn_map.fadeIn(500);
	    sidebar.animate({'margin-left':'0px'}, 700);
	}else{
	    $(e).fadeIn(500);
	}
    }

    function hideSidebar(){
	if (oni_mob){
	    oni_header_btn_map.hide();
	    oni_header_btn_i.fadeIn(500);
	    sidebar.animate({'margin-left':'-100vw'}, 700, function(){
		$('.selected').removeClass('selected');		
	    });

	}
    }
    
    sidebar.on('swipeleft click', hideSidebar);
    oni_header_btn_map.on('click', hideSidebar);
    
    var markers = L.markerClusterGroup({
	maxClusterRadius: 20,
	iconCreateFunction: function (cluster) {
	    var markers = cluster.getAllChildMarkers();
	    var n = 0;
	    for (var i = 0; i < markers.length; i++) {
		n += 1;
	    }
	    
	    return L.divIcon({
		html: '<div class="onicluster-data">' + n + '</div>',
		className: 'onicluster',
		iconSize:     [w_icons, h_icons], 
		iconAnchor:   [w_icons/2, h_icons]
	    });
	}
    });
    
    oni_posts.each(function(e){
	var oni_id = parseFloat($(this).find('.oni-id').html());
	var oni_posttype = $(this).find('.oni-posttype').text().trim();
	var oni_postname = $(this).find('.oni-postname').text().trim();

	
	var oni_elaboreve = $(this).find('article').hasClass('tag-elaboreve')
	if (oni_elaboreve){
	    console.log('elaboreve');
	}
	
	if (oni_posttype == 'bateau'){
	    // var oni_lat = larochelle[0];
	    // var oni_long = larochelle[1];
	    var oni_lat = 46.687835;
	    var oni_long = -2.387339;

	}else{
	    var oni_lat = parseFloat($(this).find('.oni-lat').html());
	    var oni_long = parseFloat($(this).find('.oni-long').html());
	}
	    
	var this_post = this;
	
	posts[oni_postname] = {
	    'post':this,
	    'lat': oni_lat,
	    'long':oni_long,
	    'id':oni_id,
	    'posttype':oni_posttype,
	    'postname':oni_postname
	}

	var is_selected = false;
	
	if (oni_postname === hash_postname){
	    showPost(this_post);
	    mymap.setView([oni_lat, oni_long], 11);
	    is_selected = true;

	    console.log("hash_postname");
	    console.log(oni_posttype);
	    console.log(hash_postname);
	}

	if (oni_postname == oni_single_post){
//	    showPost(this_post);
	    mymap.setView([oni_lat, oni_long], 9);
	    is_selected = true;
	    console.log("oni single post");
	}
	
	var marker;
	marker = L.marker([oni_lat, oni_long] , {icon: createPostIcon(oni_posttype, oni_id, is_selected, oni_elaboreve)});	
	marker.on('mouseover', function(ev){
	    if (oni_main){
		$('.onimarker').removeClass('selected');
		$('.onimarker-'+oni_id).addClass('selected');
		showPost(this_post);
	    }
	});

	marker.on('click', function(ev){
	    if (oni_main){
		$('.onimarker').removeClass('selected');
		$('.onimarker-'+oni_id).addClass('selected');
		location.hash = '#'+oni_postname;
		showPost(this_post);
	    }else{
		var new_url = WPURLS['siteurl'] + '/' + oni_frontpage + '/#' + oni_postname;
		window.location.replace(new_url);
	    }
	});

	if (oni_posttype != 'bateau'){
	    markers.addLayer(marker);
	}else{
	    marker.addTo(mymap);
	    bateau_marker = marker;
	}
    });
    
    mymap.addLayer(markers);


    //    var openpath_data_url = WPURLS.siteurl + "/openpath/openpath_data.json";
    // var openpath_data_url = WPURLS.siteurl + "/openpath/openpath_data_temp.json";
    // $.getJSON(openpath_data_url, function(data){
    // 	var pt_list = [];

    // 	data.forEach(function(d){
    // 	    pt_list.push(new L.LatLng(d.lat, d.lon));
    // 	});

    // 	var firstpolyline = new L.Polyline(pt_list, {
    // 	    color: 'blue',
    // 	    weight: 3,
    // 	    opacity: 1,
    // 	    smoothFactor: 1,
    // 	    dashArray:"1, 10"

    // 	});
    // 	firstpolyline.addTo(mymap);

    // 	console.log(pt_list[pt_list.length-1]);
    // 	var latlon = new L.LatLng(pt_list[pt_list.length-1].lat, pt_list[pt_list.length-1].lng);
    // 	bateau_marker.setLatLng(latlon);
    // });

    gps_dat = $('#oni-gps p').text();

    var pt_list = []
    gps_dat.trim().split("\n").forEach(function(d){
	var ss = d.split(' ');
	var date_pos = new Date(ss[0]);
	if (date_pos < Date.now()){
	    //	    pt_list.push({'date': date_pos, 'lat': parseFloat(ss[1]), 'lon':parseFloat(ss[2])});
	    pt_list.push(new L.LatLng(parseFloat(ss[1]), parseFloat(ss[2])));
	}
    });

    var firstpolyline = new L.Polyline(pt_list, {
	color: 'blue',
	weight: 3,
	opacity: 1,
	smoothFactor: 1,
	dashArray:"1, 10"
	
    });
    firstpolyline.addTo(mymap);
    
    var latlon = new L.LatLng(pt_list[pt_list.length-1].lat, pt_list[pt_list.length-1].lng);
    bateau_marker.setLatLng(latlon);

    var pos_bateau = mymap.latLngToContainerPoint(latlon);
    
    $('article.format-chat').find('.entry-footer').hide();
    $('article.format-chat').find('.entry-title').prepend('<div class="oni-pictoconcert"></div>');
    
    $('.custom-logo').on('mouseover', function(ev){
	oni_posts.each(function(el){
	    $(this).hide();
	});
	page_content.fadeIn(700);
    })
    
    $(window).on("resize", function() {

	if ($(document).width() < oni_mob_w){
	    oni_mob = true;
	}else{
	    oni_mob = false;
	    sidebar.css('margin-left', '0px');
	}

	$('#mapid').width($('#primary').width());
	$('#mapid').height($('#primary').height());
	mymap.invalidateSize();
    }).trigger("resize");

    var sketch = function(p) {
	var r = 200;
	var max_r = 200;

	var r_start = 200;

	var t_start = -1;
	
	var crosses = [];
	var delays = [];

	var n_mart = 8;

	var do_draw;
	
	p.dashedLine = function(l){
	    var e = 4;
	    for (var i = 0; i < l; i = i+2*e){
		p.line(i+e, 0, i+2*e, 0);
		p.line(-i+e, 0, -i+2*e, 0);
	    }
	}
	
	p.drawStar = function(j){
	    p.push();
	    for (var i = 1; i<16+1 ;i ++){

		p.rotate(360.0/32.0);

		if (crosses[j][i][1] < p.width){
		    crosses[j][i][1] += crosses[j][i][0];
		}
		

//		console.log(p.millis(), delays[j]);
		var r = crosses[j][i][1];
		if (i%4==0){
		    p.strokeWeight(0.8);
		    p.stroke(210);
		    p.line(-r, 0, r, 0);
		}else if (i%2){
		    p.strokeWeight(0.5);
		    p.stroke(160);
		    p.line(-r, 0, r, 0);
		}else{
		    p.strokeWeight(0.5);
		    p.stroke(100);
		    	    p.line(-r, 0, r, 0);
		    //p.dashedLine(r);
		}

	    }
	    p.pop()
	}
	
	p.setup = function(){
	    t_start = p.millis();
	    if (p.random(1.0) >= 0.5){
		n_mart = 16;
	    }
	    do_draw = true;
	    p.createCanvas($('#primary').width(), $('#primary').height());
	    p.background(0);
	    p.angleMode(p.DEGREES);
	    p.frameRate(15);
	    for (var i = 0; i < 17; i++){
		var cross_i = []
		for (var j = 0; j < 17;j++){
		    var s = 2 + p.random(3);
		    if (j%4!=0){
			s = 0.5+p.random(1.5);
		    }
		    cross_i.push([s, 0]);
		}
		crosses.push(cross_i);
	    }
//	    console.log(crosses);
	    delays.push(0);
	    for (var j = 0; j < n_mart;j++){
		delays.push(6000+p.random(2000));
	    }
//	    console.log(delays);
	}

	p.restart = function(){

	    crosses = [];
	    delays = [];
	    do_draw = true;

	    t_start = p.millis();
	    if (p.random(1.0) >= 0.5){
		n_mart = 16;
	    }
	    
	    for (var i = 0; i < 17; i++){
		var cross_i = []
		for (var j = 0; j < 17;j++){
		    var s = 2 + p.random(3);
		    if (j%4!=0){
			s = 0.5+p.random(1.5);
		    }
		    cross_i.push([s, 0]);
		}
		crosses.push(cross_i);
	    }
//	    console.log(crosses);
	    delays.push(0);
	    for (var j = 0; j < n_mart;j++){
		delays.push(6000+p.random(2000));
	    } 
	}

	p.pause = function(){
	    do_draw = false;
	}
	
	p.draw = function(){
	    // if (r < max_r*2){
	    // 	r += 1;
	    // }
	    p.background(0);
	    if (do_draw){
		p.stroke(128);
		p.strokeWeight(0.3);

		p.push();
		pos_bateau = mymap.latLngToContainerPoint(latlon);
		p.translate(pos_bateau.x, pos_bateau.y);
		p.drawStar(0);
		
		for(var i = 0 ; i<n_mart; i++){
		    if (p.millis()-t_start > delays[i+1]){
			p.push();
	    		p.rotate(360*i/n_mart);
	    		p.translate(-max_r*2, 0);
	    		p.rotate(-360*i/n_mart);
	    		p.drawStar(i+1);
	    		p.pop();
		    }
		}
		
		p.pop();
	    }
	}
    };
    
    pp = new p5(sketch, 'p5jscontainer');
    
    mymap.on('zoomstart', function(){
    	pp.pause();
    });
    
    mymap.on('zoomend', function() {
	pp.restart();
//    	pp = new p5(sketch, 'p5jscontainer');
    	pos_bateau = mymap.latLngToContainerPoint(latlon);
    });

    
});

