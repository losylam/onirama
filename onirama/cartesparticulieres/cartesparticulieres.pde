///////////////////////////////////////////////
// from Beesandbombs
int[][] result;
float t, c;

boolean recording = true;

int samplesPerFrame = 1;
int numFrames = 80;
//int numFrames = 4;        
float shutterAngle = 1;


float ease(float p) {
  return 3*p*p - 2*p*p*p;
}

float ease(float p, float g) {
  if (p < 0.5) 
    return 0.5 * pow(2*p, g);
  else
    return 1 - 0.5 * pow(2*(1 - p), g);
}


void draw() {
  
  if (!recording) {
    t = mouseX*1.0/width;
    c = mouseY*1.0/height;
    if (mousePressed)
      println(c);
    draw_();
  } else {
    for (int i=0; i<width*height; i++)
      for (int a=0; a<3; a++)
        result[i][a] = 0;
    
    c = 0;
    for (int sa=0; sa<samplesPerFrame; sa++) {
      t = map(frameCount-1 + sa*shutterAngle/samplesPerFrame, 0, numFrames, 0, 1);
      //      println(frameCount-1 + sa*shutterAngle/samplesPerFrame)
      draw_();
      loadPixels();
      for (int i=0; i<pixels.length; i++) {
        result[i][0] += pixels[i] >> 16 & 0xff;
        result[i][1] += pixels[i] >> 8 & 0xff;
        result[i][2] += pixels[i] & 0xff;
      }
    }
    
    loadPixels();
    for (int i=0; i<pixels.length; i++)
      pixels[i] = 0xff << 24 | 
        int(result[i][0]*1.0/samplesPerFrame) << 16 | 
        int(result[i][1]*1.0/samplesPerFrame) << 8 | 
        int(result[i][2]*1.0/samplesPerFrame);
    updatePixels();

    saveFrame("f###.gif");

    //if (frameCount==1)
    if (frameCount==numFrames)
      exit();
  }
}

////////////////////////////////////////////////////

float h;
float w;


int n_cross = 64;

void setup(){
  println(true? "1": "2");
  size(1000, 1000);
  smooth(8);
  result = new int[width*height][3];
  print(TWO_PI);
}

void draw_(){
  background(0);



  pushMatrix();
  translate(width/2, height/2);
  rotate(PI*t/(n_cross/2));
  w = width;
  h = height;
  
  stroke(255);
  strokeWeight(1.5);
  //  drawGrid(100, 4);
  println(ease(t)*w/4);
  
  //  drawCross(n_cross);
  for (int i = 0; i < n_cross; i++){
    rotate(TWO_PI/n_cross);
    pushMatrix();
    translate(ease(t)*w/2, 0);
    drawCross(n_cross);
    popMatrix();
  }
  
  if (t>0.0){
    drawCrossL(n_cross, map(t, 0.0, 1, w, 0));
  }
  popMatrix();
}

void drawGrid(int step, int n){
  for (int i = -n ; i < n+1 ; i++){
    line(-w/2, i*step, w*3/4, i*step);
    line(i*step, -h/2, i*step, h/2);
  }
}

void drawCross(int n){
  pushMatrix();
  for (int i = 1; i < n+1; i++){
    // if (i%4 == 0){
    //   stroke(255);
    // }else if (i%2 == 0){
    //   stroke(10);
    // }else{
    //   stroke(90);
    // }
    float l = w*6;
    if (i%2 == 0){
      stroke(255);
    }else{
      stroke(200, 0, 0);
      l = w;
    }
    rotate(PI/n);
    line(-l, 0, l, 0);
  }
  popMatrix();
}

void drawCrossL(int n, float l){
  pushMatrix();
  for (int i = 1; i < n+1; i++){
    // if (i%4 == 0){
    //   stroke(255);
    // }else if (i%2 == 0){
    //   stroke(10);
    // }else{
    //   stroke(90);
    // }
    stroke(255);
    if (i%2 == 0){
      stroke(255);
    }else{
      stroke(200, 0, 0);
    }

    rotate(PI/n);
    line(-w, 0, -l, 0);
    line(w, 0, l, 0);
  }
  popMatrix();
}


void keyPressed(){
}
