var mymap;
var oni_posts;

// window.onload = function(){
//     initLeaflet();   
// }

jQuery(document).ready(function($) {
    // Code that uses jQuery's $ can follow here.
    initLeaflet($);
});

initLeaflet = function($){
    var mymap = L.map('mapid', {zoomControl: false}).setView([47.4, -2.5], 8);
    new L.Control.Zoom({ position: 'bottomright' }).addTo(mymap);
    var tonerUrl = "http://{S}tile.stamen.com/toner/{Z}/{X}/{Y}.png";
//    var tonerUrl = "https://api.mapbox.com/v4/laurentmalys.9iobprs6/{Z}/{X}/{Y}.png?access_token=pk.eyJ1IjoibGF1cmVudG1hbHlzIiwiYSI6ImNqZ2NjbjFnNjN6OXEyenBoeDZsYTI3cmMifQ.9TuU9MbZFAhnZwh9xrgU0w"
 //   var tonerUrl = "https://api.mapbox.com/v4/laurentmalys.2bw9mm02/{Z}/{X}/{Y}.png?access_token=pk.eyJ1IjoibGF1cmVudG1hbHlzIiwiYSI6ImNqZ2NjbjFnNjN6OXEyenBoeDZsYTI3cmMifQ.9TuU9MbZFAhnZwh9xrgU0w"
    var url = tonerUrl.replace(/({[A-Z]})/g, function(s) {
	return s.toLowerCase();
    });
    
    jQuery('#mapid').width(jQuery('#primary').width());
    jQuery('#mapid').height(jQuery('#primary').height());
    mymap.invalidateSize();    

    L.tileLayer(url, {
	subdomains: ['','a.','b.','c.','d.'],
	attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="http://maps.stamen.com">Stamen</a> Toner',
	maxZoom: 18,
	id: 'mapbox.streets'// ,
	// accessToken: 'your.mapbox.access.token'
    }).addTo(mymap);

    var pointList = []

    pointList.push(new L.LatLng(46.145, -1.1782)); //La Rochelle

    46.271,-3.032
    pointList.push(new L.LatLng(48.3973101, -4.9570331)); //Molène 
    pointList.push(new L.LatLng(48.03919, -4.84894)); //Sein
    pointList.push(new L.LatLng(47.27479,-2.20272)); //St Nazaire
    pointList.push(new L.LatLng(47.20492,-1.56386)); //Nantes
    
    var larochelle = [46.145, -1.1782];
    var molene = [48.3973101, -4.9570331];
    var sein = [48.03919, -4.84894];
    var saintnazaire = [47.27479,-2.20272];
    var nantes = [47.20492,-1.56386];
    function getCurveBetween(latlng1, latlng2, thetaOffset){
	
	var latlngs = [];
	
	var pathOptions = {
	    color: 'white',
	    weight: 1.5,
	    opacity: 1,
	    smoothFactor: 1,
	    dashArray:"1, 10"
	};

	var offsetX = latlng2[1] - latlng1[1],
	offsetY = latlng2[0] - latlng1[0];
	
	var r = Math.sqrt( Math.pow(offsetX, 2) + Math.pow(offsetY, 2) ),
	theta = Math.atan2(offsetY, offsetX);
	
//	var thetaOffset = (3.14/5.5);
	
	var r2 = (r/2)/(Math.cos(thetaOffset));
	var theta2 = theta + thetaOffset;
	
	var midpointX = (r2 * Math.cos(theta2)) + latlng1[1],
	midpointY = (r2 * Math.sin(theta2)) + latlng1[0];
	
	var midpointLatLng = [midpointY, midpointX];
	
	latlngs.push(latlng1, midpointLatLng, latlng2);

	var curvedPath = L.curve(['M', latlng1,
				  'Q', midpointLatLng,
				  latlng2], pathOptions);
	return curvedPath;
    }

    // var dep = L.curve(['M', larochelle,
    // 		       'C', [46.248,-2.450], larochelle, [46.999,-2.928],
    // 		       'S', [46.999,-2.928], [48.012,-4.966],
    // 		       'S', [48.012,-4.966], molene], {color:'red'})
    // dep.addTo(mymap);

    
    var l0 = getCurveBetween(larochelle, molene, 3.14/5.5);
    l0.addTo(mymap);
    var l1 = getCurveBetween(molene, sein, -3.14/5.5);
    l1.addTo(mymap);
    var l2 = getCurveBetween(sein, [47.2389,-2.2110], -3.14/5.5);
    l2.addTo(mymap);
    var l2 = getCurveBetween([47.2389,-2.2110],saintnazaire, -3.14/5.5);
    l2.addTo(mymap);
    var l2 = getCurveBetween(saintnazaire, [47.2622,-1.8591], 3.14/7);
    l2.addTo(mymap);

    var l2 = getCurveBetween([47.2622,-1.8591], nantes, -3.14/9);
    l2.addTo(mymap);



    var firstpolyline = new L.Polyline(pointList, {
	color: 'blue',
	weight: 2,
	opacity: 1,
	smoothFactor: 1
    });
//    firstpolyline.addTo(mymap);
			     

    oni_posts = jQuery('.oni-post');

//    alert(templateUrl);
    
    var postIcons = {};
    var posttypes = ["video", "audio", "image", "chat"];
    var postIconsScale = 0.35;
    var w_icons = 100 * postIconsScale;
    var h_icons = 160 * postIconsScale;

    posbateau =  L.icon({
	iconUrl: templateUrl+"/onirama/pictos/pictofondb_bateau.png",
	iconSize:     [w_icons, h_icons], // size of the icon
	iconAnchor:   [w_icons/2, h_icons], // point of the icon which will correspond to marker's location
	popupAnchor:  [-3, -76] // point from which the popup should open relative to the iconAnchor
    });
    
    var marker = L.marker(larochelle, {icon: posbateau}).addTo(mymap);	

			     


    posttypes.forEach(function(e){
	var i = L.icon({
	    iconUrl: templateUrl+"/onirama/pictos/pictofondb_"+e+".png",
	    iconSize:     [w_icons, h_icons], // size of the icon
	    iconAnchor:   [w_icons/2, h_icons], // point of the icon which will correspond to marker's location
	    popupAnchor:  [-3, -76] // point from which the popup should open relative to the iconAnchor
	});
	postIcons[e] = i;
    });
    
    postnotypes =  L.icon({
	iconUrl: templateUrl+"/onirama/pictos/pictofondb_vide.png",
	iconSize:     [w_icons, h_icons], // size of the icon
	iconAnchor:   [w_icons/2, h_icons], // point of the icon which will correspond to marker's location
	popupAnchor:  [-3, -76] // point from which the popup should open relative to the iconAnchor
    });

    var sidebar = $('#sidebar');

    //elem.addClass("secondary");
    //sidebar.append( jQuery("div").addClass("secondary").attr('id', "oni-post") ) ;
    sidebar.append('<div id="oni-post"> </div>') ;

    var onipost = $('#oni-post').addClass("secondary");

    var lastpost;

    var page_content =	$('#page-content');

    page_content.appendTo(sidebar);
	$('#secondary').hide();

    $('#oni-posts').appendTo(onipost);

    $('.custom-logo').on('mouseover', function(ev){
	oni_posts.each(function(el){
	    $(this).hide();
	});
	page_content.show(700);
    })

    function showPost(e){
	page_content.hide();
	oni_posts.each(function(el){
	    $(this).hide();
	});
	$(e).fadeIn(700);
	console.log(e);
    }
    
    oni_posts.each(function(e){
	var oni_lat = parseFloat(jQuery(this).find('.oni-lat').html());
	var oni_long = parseFloat(jQuery(this).find('.oni-long').html());
	var oni_posttype = jQuery(this).find('.oni-posttype').text().trim();
	var oni_content = jQuery(this).find('.oni-content');

	var this_post = this;

	var marker;
	if (oni_posttype == "chat"){
	    var marker = L.marker([oni_lat, oni_long] , {icon: postIcons[oni_posttype]}).addTo(mymap);
	}
	
	// }else{
	//     var marker = L.marker([oni_lat, oni_long], {icon: postnotypes}).addTo(mymap);	
	// }
	// marker.on('mouseover', function(ev){
	//     console.log(oni_posttype);
	//     showPost(this_post);
	// });


    });
    

    jQuery(window).on("resize", function() {
	jQuery('#mapid').width(jQuery('#primary').width());
	jQuery('#mapid').height(jQuery('#primary').height());
	mymap.invalidateSize();
    }).trigger("resize");
}

