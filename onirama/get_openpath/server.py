#!/env/python
# coding: utf-8

import oauth2, urllib, urllib2, time

from flask import Flask, render_template, request, jsonify, redirect, url_for
import json
import random
import datetime

ACCESS = "HN5M6LQAIUQ4S6TYFUXEYL5MUY"
SECRET = "P2UTPALB45NMP9CQTJHT2ZZXG00JTH0C0555E515ANQZXIQYH7BT0T0N9UO716LB"
URL = "https://openpaths.cc/api/1" 

def build_auth_header(url, method):
    params = {                                            
        'oauth_version': "1.0",
        'oauth_nonce': oauth2.generate_nonce(),
        'oauth_timestamp': str(int(time.time())),
    }
    consumer = oauth2.Consumer(key=ACCESS, secret=SECRET)
    params['oauth_consumer_key'] = consumer.key 
    request = oauth2.Request(method=method, url=url, parameters=params)    
    signature_method = oauth2.SignatureMethod_HMAC_SHA1()
    request.sign_request(signature_method, consumer, None)
    return request.to_header()

    
def get_openpath():
    params = {'num_points': 10}    # get the last 24 hours
    query = "%s?%s" % (URL, urllib.urlencode(params))
    print(query)
    try:
        request = urllib2.Request(query)
        request.headers = build_auth_header(URL, 'GET')
        connection = urllib2.urlopen(request)
        data = json.loads(''.join(connection.readlines()))
        print(json.dumps(data, indent=4))
        json.dump(data, open('opanpath_data.json', 'w'));
        return json.dumps(data, indent=4)
    except urllib2.HTTPError as e:
        print(e.read())    
        return None

app = Flask(__name__)
    
@app.route('/')
def index():
    a = get_openpath()
    return a



if __name__ == '__main__':
    app.run(debug=True)
    
